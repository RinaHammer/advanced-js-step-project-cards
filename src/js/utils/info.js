export const info = {
    cardiologist: "Кардіолог",
    dentist: "Стоматолог",
    therapist: "Терапевт",

    urgency: [
        "Звичайна",
        "Пріоритетна",
        "Невідкладна",
    ],

    doctors: [
        "Кардіолог",
        "Стоматолог",
        "Терапевт",
    ],
}