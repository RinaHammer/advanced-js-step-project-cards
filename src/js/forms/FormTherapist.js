import Input from "../components/Input";
import {Form} from "./Form";
import {info} from "../utils/info";

export class FormTherapist extends Form {
    constructor() {
        super(info.therapist);
        this.age = new Input('Вік:', 'text', 'Вік', false,'form-control', 'age');
 }
    render(){
        super.render();
        this.self.append(this.age.render());

        return this.self;
    }

    renderForEdit(id, purpose, description, urgency, patientName, age){
        super.renderForEdit(id, purpose, description, urgency, patientName);

        this.age.value = age;

        this.self.append(this.age.render());

        return this.self;
    }
}