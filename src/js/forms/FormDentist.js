import Input from "../components/Input";
import {info} from "../utils/info";
import {Form} from "./Form";

export class FormDentist extends Form {
    constructor() {
        super(info.dentist);
        this.lastVisit = new Input('Дата останнього візиту:', 'date', ' ', false,'last-visit', 'lastVisit');
    }

    render(){
        super.render();
        this.self.append(this.lastVisit.render());

        return this.self;
    }

    renderForEdit(id, purpose, description, urgency, patientName, lastVisit){
        super.renderForEdit(id, purpose, description, urgency, patientName);

        this.lastVisit.value = lastVisit;

        this.self.append(this.lastVisit.render());

        return this.self;
    }
}

