import Select from "../components/Select";
import {info} from "../utils/info";
import Input from "../components/Input";
import Textarea from "../components/Textarea";

export class Form{
    constructor(doctor) {
        this.self = document.createElement('form');

        let doctorInput = new Input("", "text", "", false, "form-control", "doctor", [{name:"readonly", value: ""}, {name:"hidden", value:""}]);
        doctorInput.value = doctor;

        this.doctor = doctorInput;

        this.purpose = new Input('Ціль візиту:','text', 'Ціль візиту', true,'form-control', 'purpose');
        this.description = new Textarea('Опис візиту:', '3', 'Опис візиту', true, 'form-control', 'description');
        this.urgency = new Select(info.urgency,'Терміновість',true, 'form-select', 'urgency');
        this.patientName = new Input('ПІБ:','text', 'ПІБ', true,'form-control', 'patientName');
    }
    render(){
        this.self.append(
            this.doctor.render(),
            this.purpose.render(),
            this.description.render(),
            this.urgency.render(),
            this.patientName.render());

        return this.self;
    }

    renderForEdit(id, purpose, description, urgency, patientName){

        let hiddenId = new Input("", "hidden", "", false, "hidden-edit-id", "id");
        hiddenId.value = id;

        this.purpose.value = purpose;
        this.description.value = description;
        this.urgency.value = urgency;
        this.patientName.value = patientName;

        this.self.append(
            hiddenId.render(),
            this.doctor.render(),
            this.purpose.render(),
            this.description.render(),
            this.urgency.render(),
            this.patientName.render());

        return this.self;
    }
}