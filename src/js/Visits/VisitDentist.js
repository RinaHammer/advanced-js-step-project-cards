import {Visit} from "./Visit";

export class VisitDentist extends Visit {
    constructor(id, patientName, doctor, purpose, description, urgency, lastVisit) {
        super(id, patientName, doctor, purpose, description, urgency);

        this.data = {
            ...this.data,
            lastVisit,
        };
    }
    render(){
        super.render();

        this.lastVisit = document.createElement('p');
        this.lastVisit.innerText = `Останній візит: ${this.data.lastVisit}`;

        this.showMoreInfo.append(this.lastVisit);

        return this.wrapper;
    }
}
