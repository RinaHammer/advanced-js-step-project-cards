import "./Modal-login";
import "./loginInput";
import { ModalVisit } from "./modals/ModalVisit";
import renderCards from "./modals/ModalVisit";
import { dropToken, getToken } from "./utils/token";
import "./filters/inputFilter";

const logOutBtn = document.querySelector(".btn-exit");
const btnCreateVisit = document.querySelector(".btn-create");

export var modal;

document.addEventListener("DOMContentLoaded", () => {
  if (getToken()) {
    modal = new ModalVisit(document.body);
    renderCards();

    let bntAuthorised = document.querySelector(".btns-authorised");
    bntAuthorised.classList.add("show");

    let btnNonAuthorised = document.querySelector(".btn-non-authorised");
    btnNonAuthorised.classList.remove("show");

    let filter = document.querySelector(".filter__block");
    filter.style.display = "flex";
  } else {
    let bntAuthorised = document.querySelector(".btns-authorised");
    bntAuthorised.classList.remove("show");

    let btnNonAuthorised = document.querySelector(".btn-non-authorised");
    btnNonAuthorised.classList.add("show");

    let hiddenFilter = document.querySelector(".filter__block");
    hiddenFilter.style.display = "none";
  }
});

btnCreateVisit.addEventListener("click", (e) => {
  e.preventDefault();
  modal.show();
});

logOutBtn.addEventListener("click", (e) => {
  e.preventDefault();
  dropToken();
  document.location.href = "./index.html";
});
